from flask import Flask
from resources import api
from utils.db import db
from utils.mail import mail
from flask_cors import CORS
from flask_jwt import JWT
from models.employee_model import Employee
app = Flask(__name__)

cors = CORS(app, resources={r"/*/*": {"origins": r"*"}})
api.init_app(app)
app.config['UPLOAD_FOLDER'] = '/files/'
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql:///employee'
app.config['SECRET_KEY']="secretkey"

app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = 'hrms.work.11@gmail.com'
app.config['MAIL_DEFAULT_SENDER'] = 'hrms.work.11@gmail.com'
app.config['MAIL_PASSWORD'] = '123@qwer'
app.config['MAIL_ASCII_ATTACHMENTS'] = True

from utils.security import *
jwt = JWT(app,authentiction,identity)

with app.app_context():
    db.init_app(app)
    db.create_all()

if __name__ == "__main__":
    mail.init_app(app)
    app.run(debug=True)